package fintech.middle.jpa.model.v2;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "v2Courses")
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue
    Long id;

    String courseName;

    @OneToMany(mappedBy = "course")
    List<Student> studentsApplied = new ArrayList<>();

    LocalDate endDate = null;

    public Course() {
    }

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public void addStudent(Student student) {
        studentsApplied.add(student);
        student.setCourse(this);
    }

    public void removeStudent(Student student) {
        studentsApplied.remove(student);
        student.setCourse(null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public List<Student> getStudentsApplied() {
        return studentsApplied;
    }

    public void setStudentsApplied(List<Student> studentsApplied) {
        this.studentsApplied = studentsApplied;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
