package fintech.middle.jpa.model.v3;

public interface NamedEntity {
    String getName();
}
