package fintech.middle.jpa.model.v3;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "v3Courses")
@Table(name = "courses3")
@Cacheable
public class Course {
    @Id
//    @SequenceGenerator(name="seq_courses")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "seq_course3"
    )
    @SequenceGenerator(
            name = "seq_course3",
            sequenceName = "seq_course3",
            allocationSize = 1
    )
    Long id;

    String courseName;

    @ManyToMany( mappedBy = "courses")
    @Fetch(FetchMode.SELECT)
    List<Student> studentsApplied = new ArrayList<>();

    @ManyToOne
    Lecturer lecturer;

    LocalDate endDate = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public List<Student> getStudentsApplied() {
        return studentsApplied;
    }

    public void setStudentsApplied(List<Student> studentsApplied) {
        this.studentsApplied = studentsApplied;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", courseName='" + courseName + '\'' +
                ", lecturer=" + lecturer +
                '}';
    }
}
