package fintech.middle.jpa.model.v3;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.stream.Collectors;

public interface StudentView {
    String getName();

    default String formatToString() {
        return "Student = " + getName() + " ,courses=[" +
                "]";
    }

}

