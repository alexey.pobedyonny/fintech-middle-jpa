package fintech.middle.jpa.model.v3;

import org.springframework.beans.factory.annotation.Value;

public interface StudentViewFull {
    String getName();

    String getFamilyName();
}
