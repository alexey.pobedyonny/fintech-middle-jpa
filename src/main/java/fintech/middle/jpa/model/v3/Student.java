package fintech.middle.jpa.model.v3;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "v3Student")
@Table(name = "students3")
public class Student {
    @Id
    @GeneratedValue(generator = "seq_students")
    @SequenceGenerator(name="seq_students")
    Long id;

    @Column
    String name;

    String familyName;

    @LastModifiedDate
    LocalDateTime modifyDate;

    @CreatedDate
    LocalDateTime creationDate;

    @ManyToMany
    List<Course> courses = new ArrayList<>();

    @ManyToOne
    Student curator;

    @OneToMany(
            mappedBy = "curator"
    )
    List<Student> curated = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(LocalDateTime modifyDate) {
        this.modifyDate = modifyDate;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }


    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", familyName='" + familyName + '\'' +
                ", modifyDate=" + modifyDate +
                ", creationDate=" + creationDate +
                ", courses=" + courses +
                '}';
    }
}
