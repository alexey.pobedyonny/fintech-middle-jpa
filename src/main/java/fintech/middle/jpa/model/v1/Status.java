package fintech.middle.jpa.model.v1;

public enum Status {
    SUBSIDIARY, EXCHANGE_STUDENT, ATTENDING, LEFT
}
