package fintech.middle.jpa.model.v1;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
abstract class NamedEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
