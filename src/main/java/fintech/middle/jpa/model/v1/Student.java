package fintech.middle.jpa.model.v1;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "v1Student")
@Table(name = "students1")
@EntityListeners(AuditingEntityListener.class)
public class Student extends NamedEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "phone_num", nullable = true, unique = true)
    private String phoneNumber;

    @Enumerated
    private Status status;

    @LastModifiedDate
    private LocalDateTime modifyDate;

    @CreatedDate
    private LocalDateTime creationDate;

    public Long getId() {
        return id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(LocalDateTime modifyDate) {
        this.modifyDate = modifyDate;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
