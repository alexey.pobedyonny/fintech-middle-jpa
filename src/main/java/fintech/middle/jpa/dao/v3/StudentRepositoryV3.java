package fintech.middle.jpa.dao.v3;

import fintech.middle.jpa.model.v3.NamedEntity;
import fintech.middle.jpa.model.v3.Student;
import fintech.middle.jpa.model.v3.StudentView;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public interface StudentRepositoryV3 extends JpaRepository<Student, Long> {

    @Query("Update v3Student s set s.name=:name where s.id=:id")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Transactional
    int updateStudentName(Long id, String name);

    @Query("select s.name from v3Student s join s.courses c where c.courseName=:name")
    List<String> findAllStudentNamesAttendingCourse(String name);

    List<NamedEntity> findByCourses_CourseName(String name);

    <T> List<T> findStudentsByName(String name, Class<T> type);

    @Query("from v3Student s join s.courses c where c.courseName=:name")
    List<Student> findAllStudentNamesAttendingCourse(String name, Pageable page);

    @Query("from v3Student s join fetch s.curated c where c.name=:name ")
    List<Student> findAllStudentsWithCuratorByName(String name, Pageable page);

    List<Student> findByName(String name, Pageable page);
}
