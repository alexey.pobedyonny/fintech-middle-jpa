package fintech.middle.jpa.dao.v3;

import fintech.middle.jpa.model.v3.Course;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CourseServiceV3 {
    private CourseRepositoryV3 courseRepository;

    public CourseServiceV3(CourseRepositoryV3 courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW
//            ,timeout = 2
    )
    public Course waitForCourseToFinish(Long courseId) {
        var course = courseRepository.getById(courseId);
        //тут мог бы быть вызов вашего веб-сервиса
        while(course.getEndDate() == null) {
            try {
                Thread.sleep(5_000);
                System.out.println("Still waiting for course to finish in thread " + Thread.currentThread().getName());
            } catch (InterruptedException ignored) {}
        }
        return course;
    }
}
