package fintech.middle.jpa.dao.v3;

import com.github.javafaker.Faker;
import fintech.middle.jpa.model.v3.Course;
import fintech.middle.jpa.model.v3.Lecturer;
import fintech.middle.jpa.model.v3.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class CoursePlanningService {
    public static final int LECTURERS_COUNT = 10;
    public static final int STUDENTS_COUNT = 100;
    public static final int COURSES_COUNT = 20;
    private CourseRepositoryV3 courseRepository;
    private StudentRepositoryV3 studentRepository;
    private LecturerRepositoryV3 lecturerRepositoryV3;
    private Faker faker = new Faker();

    public CoursePlanningService(CourseRepositoryV3 courseRepository, StudentRepositoryV3 studentRepository, LecturerRepositoryV3 lecturerRepositoryV3) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
        this.lecturerRepositoryV3 = lecturerRepositoryV3;
    }

    public void planStudentsForCourses() {
        var lecturers = recreateLecturersIfWeForgotSomeone();
        var students = enlistStudents();
        var courses = planCourses(lecturers);
        appointStudentsToCourses(students, courses);
    }

    private void appointStudentsToCourses(List<Student> students, List<Course> courses) {
        students.forEach(student -> {
            for(int i=0; i<=ThreadLocalRandom.current().nextInt(5);i ++)
            student.getCourses().add(randomItem(courses));
            studentRepository.save(student);
        });
    }

    private List<Course> planCourses(List<Lecturer> lecturers) {
        List<Course> courses = new ArrayList<>(COURSES_COUNT);
        for (int i = 0; i < COURSES_COUNT; i++) {
            Course course = new Course();
            course.setCourseName(faker.educator().course());
            course.setLecturer(randomItem(lecturers));
            courses.add(courseRepository.save(course));
        }
        return courses;
    }

    private List<Student> enlistStudents() {
        List<Student> studentsList = new ArrayList<>(STUDENTS_COUNT);
        for (int i = 0; i < STUDENTS_COUNT; i++) {
            Student student = new Student();
            student.setName(faker.name().firstName());
            student.setFamilyName(faker.name().lastName());
            studentsList.add(studentRepository.save(student));
        }
        return studentsList;
    }

    private List<Lecturer> recreateLecturersIfWeForgotSomeone() {
        List<Lecturer> lecturerList = new ArrayList<>(LECTURERS_COUNT);
        for (int i = 0; i < LECTURERS_COUNT; i++) {
            Lecturer lecturer = new Lecturer();
            lecturer.setName(faker.name().fullName());
            lecturer.setPosition(faker.job().position());
            lecturerList.add(lecturerRepositoryV3.save(lecturer));
        }
        return lecturerList;
    }

    private <T> T randomItem(List<T> list) {
        return list.get(ThreadLocalRandom.current().nextInt(list.size()));
    }
}
