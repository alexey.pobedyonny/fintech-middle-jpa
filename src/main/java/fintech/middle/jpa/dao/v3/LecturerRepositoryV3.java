package fintech.middle.jpa.dao.v3;

import fintech.middle.jpa.model.v3.Lecturer;
import fintech.middle.jpa.model.v3.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.List;


@Repository
public interface LecturerRepositoryV3 extends JpaRepository<Lecturer, Long> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Lecturer getByName(String name);

    @Query("from Lecturer where position=:position")
    @QueryHints(value = {
            @QueryHint(name = "org.hibernate.cacheable", value = "true")}
    )
    List<Lecturer> getLecturersByPosition(String position);


}
