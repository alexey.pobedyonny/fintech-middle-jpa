package fintech.middle.jpa.dao.v3;

import fintech.middle.jpa.model.v3.Course;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;


@Repository
public class CourseRepositoryV3 extends SimpleJpaRepository<Course, Long> {

    private EntityManager em;

    public CourseRepositoryV3(EntityManager em) {
        super(Course.class, em);
        this.em = em;
    }



    public List<Course> findCourseByName(String name) {
        var criteraBuilder = em.getCriteriaBuilder();
        var query = criteraBuilder.createQuery(Course.class);
        var root = query.from(Course.class);

        if (name != null) {
            var predicate = criteraBuilder.equal(root.get("courseName"), name);
            query.where(predicate);
        } else {
            query.where(
                    criteraBuilder.isNull(root.get("courseName"))
            );
        }
        return em.createQuery(query).getResultList();
    }

}
