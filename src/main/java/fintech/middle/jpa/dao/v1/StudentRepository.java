package fintech.middle.jpa.dao.v1;

import fintech.middle.jpa.model.v1.Status;
import fintech.middle.jpa.model.v1.Student;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByName(String name);
    Student findByNameAndCreationDateBeforeOrderByName(String name, LocalDateTime creationDate);
    List<Student> findByStatusInOrNameStartsWith(List<Status> status, String nameStart);
    List<Student> findByNameContaining(String namePart, Pageable page);
}


