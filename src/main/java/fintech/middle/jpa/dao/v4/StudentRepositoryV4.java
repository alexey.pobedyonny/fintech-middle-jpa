package fintech.middle.jpa.dao.v4;

import fintech.middle.jpa.model.v4.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentRepositoryV4 extends JpaRepository<Student, Long>,
        RevisionRepository<Student, Long, Long> {
}


