package fintech.middle.jpa.dao.v2;

import com.github.javafaker.Faker;
import fintech.middle.jpa.model.v2.Course;
import fintech.middle.jpa.model.v2.Student;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CourseService {
    private CourseRepository courseRepository;
    private StudentRepositoryV2 studentRepository;

    public CourseService(CourseRepository courseRepository, StudentRepositoryV2 studentRepository) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Long prepareCourse() {
        Course course = new Course("Fintech.Middle JPA");
        Student student = new Student("Ivan");
        course.getStudentsApplied().add(student);

        course.getStudentsApplied().remove(0);
        Course savedCourse = courseRepository.save(course);


        Student s1 = new Student();
        s1.setName("Ivan");
        s1.setCourse(savedCourse);
        studentRepository.save(s1);

        Student s2 = new Student();
        s2.setName("Pyotr");
        s2.setCourse(savedCourse);
        studentRepository.save(s2);
        return savedCourse.getId();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Long changeCourseName() {
        Long courseId = prepareCourse();
        Course savedCourse = courseRepository.getById(courseId);
        savedCourse.setCourseName("Math");
        courseRepository.save(savedCourse);

        savedCourse.setCourseName("Mathematix");
        courseRepository.save(savedCourse);

        savedCourse.setCourseName("Mathematics");
        return courseId;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void prepareCourses() {
        Faker faker = new Faker();
        for (int i=0;i<10;i++) {
            Course course = new Course();
            course.setCourseName(faker.company().name());
            Course savedCourse = courseRepository.save(course);
            for (int j=0;j<10;j++) {
                Student s1 = new Student();
                s1.setName(faker.name().firstName());
                s1.setFamilyName(faker.name().lastName());
                s1.setCourse(savedCourse);
                studentRepository.save(s1);
            }
        }
    }

    @Transactional
    public Course findAnyMatchingCourse() {
        Course mathCourse = new Course();
        mathCourse.setCourseName("Math");
        return courseRepository
                .findOne(Example.of(mathCourse, ExampleMatcher.matchingAny()))
                .get();
    }

    @Transactional
    public Course createMathCourse() {
        Course mathCourse = new Course();
        mathCourse.setCourseName("Math");
        return  courseRepository.save(mathCourse);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW
//            ,timeout = 2
    )
    public Course waitForCourseToFinish(Long courseId) {
        var course = courseRepository.getById(courseId);
        //тут мог бы быть вызов вашего веб-сервиса
        while(course.getEndDate() == null) {
            try {
                Thread.sleep(5_000);
                System.out.println("Still waiting for course to finish in thread " + Thread.currentThread().getName());
            } catch (InterruptedException ignored) {}
        }
        return course;
    }
}
