package fintech.middle.jpa.dao.v2;

import fintech.middle.jpa.model.v2.Course;
import fintech.middle.jpa.model.v2.Student;
import org.hibernate.Session;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import java.util.List;


@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    @Query("from v2Courses c join fetch c.studentsApplied")
    List<Course> loadAllFetchStudents();

    @Query("from v2Courses")
    @EntityGraph(attributePaths = "studentsApplied", type = EntityGraph.EntityGraphType.LOAD)
    List<Course> findAllUsingEntityGraph();

}
