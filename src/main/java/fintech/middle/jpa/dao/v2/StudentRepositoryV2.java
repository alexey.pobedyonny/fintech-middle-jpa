package fintech.middle.jpa.dao.v2;

import fintech.middle.jpa.model.v2.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentRepositoryV2 extends JpaRepository<Student, Long> {
    Student findByName(String name);

}
