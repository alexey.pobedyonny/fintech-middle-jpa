package fintech.middle.jpa.dao.v2;

import fintech.middle.jpa.model.v2.Course;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
public class MathService {
    private CourseService courseService;
    public MathService(CourseService courseService) {
        this.courseService = courseService;
    }


    @Transactional
    public Course findOrCreateMathCourse() {
        try {
            return courseService.findAnyMatchingCourse();
        } catch (NoSuchElementException re) {
            var created = courseService.createMathCourse();
            System.out.println(created);
            return created;
        }
    }
}
