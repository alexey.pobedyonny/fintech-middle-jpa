package fintech.middle.jpa.v2;

import fintech.middle.jpa.dao.v2.CourseRepository;
import fintech.middle.jpa.dao.v2.CourseService;
import fintech.middle.jpa.dao.v2.MathService;
import fintech.middle.jpa.dao.v2.StudentRepositoryV2;
import fintech.middle.jpa.model.v2.Course;
import fintech.middle.jpa.model.v2.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FintechMiddleJpaApplicationTests {
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    StudentRepositoryV2 studentRepository;
    @Autowired
    EntityManager entityManager;
    @Autowired
    CourseService courseService;
    @Autowired
    TransactionTemplate transactionTemplate;
    @Autowired
    MathService mathService;

    @Test
    @Transactional
    void dirty_checking() {
        var courseId = courseService.changeCourseName();
        assertThat(courseRepository.getById(courseId).getCourseName()).isEqualTo("Mathematics");
    }

    @Test
    @Transactional(readOnly = true)
    void readonly_transaction() {
        Long courseId = courseService.prepareCourse();
        Course savedCourse = courseRepository.findById(courseId).get();
        savedCourse.setCourseName("Other random name");

        courseRepository.save(savedCourse);
        courseRepository.flush();
    }

    @Test
    @Transactional
    void o2m_and_cascade() {
        Long couseId = courseService.prepareCourse();
        Course savedCourse = courseRepository.findById(couseId).get();

        assertThat(savedCourse.getStudentsApplied().size()).isEqualTo(2);
        System.out.println(savedCourse.getStudentsApplied());

        courseRepository.delete(savedCourse);
        assertThat(studentRepository.findAll().size()).isEqualTo(0);
    }

    @Test
    @Transactional
    void lazy_loading() {
        Long courseId = courseService.prepareCourse();
        Course savedCourse = courseRepository.findById(courseId).get();

        System.out.println("Loaded course " + savedCourse.getId());
        System.out.println(savedCourse.getStudentsApplied());
    }


    @Test
    @Transactional
    void n_plus_one() {
        courseService.prepareCourses();
        List<Course> courses = courseRepository.findAll();

        courses.forEach(course -> {
            System.out.println("Loaded course " + course.getId());
            System.out.println(course.getStudentsApplied());
        });
    }

    @Test
    @Transactional
    void fetch_graph() {
        courseService.prepareCourses();
        List<Course> courses = courseRepository.findAllUsingEntityGraph();

        courses.forEach(course -> {
            System.out.println("Loaded course " + course.getId());
            System.out.println(course.getStudentsApplied());
        });
    }

    @Test
    void lazy_init_exception() {
        Long courseId = courseService.prepareCourse();
        Course savedCourse = courseRepository.findById(courseId).get();

        System.out.println("Loaded course " + savedCourse.getId());
        System.out.println(savedCourse.getStudentsApplied());
    }


    //===================================== END OF PRESENTATION EXAMPLES =========






    @Test
    @Transactional
    void fetch_mode() {
        courseService.prepareCourses();
        List<Course> courses = courseRepository.findAll();

        courses.forEach(course -> {
            System.out.println("Loaded course " + course.getId());
            System.out.println(course.getStudentsApplied());
        });
    }

    @Test
    void transaction_template() {
        Long courseId = courseService.prepareCourse();
        transactionTemplate.executeWithoutResult(tx -> {
            Course savedCourse = courseRepository.findById(courseId).get();
            savedCourse.setCourseName("Other random name");

            courseRepository.save(savedCourse);
            courseRepository.flush();
            System.out.println("Loaded course " + savedCourse.getId());
            System.out.println(savedCourse.getStudentsApplied());
        });
    }

    @Test
    void transaction_not_working_due_to_proxy() {
        Long courseId = courseService.prepareCourse();
        loadCourseAndLogStudents(courseId);
    }

    private void loadCourseAndLogStudents(Long courseId) {
        Course savedCourse = courseRepository.findById(courseId).get();

        System.out.println("Loaded course " + savedCourse.getId());
        System.out.println(savedCourse.getStudentsApplied());
    }

    @Test
    void nested_transaction_rollback() {
        Course mathCourse = mathService.findOrCreateMathCourse();

        assertThat(mathCourse).isNotNull();
    }
}
