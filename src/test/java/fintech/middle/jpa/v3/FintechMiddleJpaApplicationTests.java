package fintech.middle.jpa.v3;

import fintech.middle.jpa.dao.v2.CourseService;
import fintech.middle.jpa.dao.v3.*;
import fintech.middle.jpa.model.v3.*;
import org.hibernate.StaleObjectStateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SuppressWarnings("unused")
@SpringBootTest
//@DataJpaTest
class FintechMiddleJpaApplicationTests {
    @Autowired
    CoursePlanningService coursePlanningService;
    @Autowired
    StudentRepositoryV3 studentRepository;
    @Autowired
    CourseRepositoryV3 courseRepository;
    @Autowired
    LecturerRepositoryV3 lecturerRepository;
    @Autowired
    TransactionTemplate transactionTemplate;
    @Autowired
    CourseServiceV3 courseService;


    @PostConstruct
    void setup() {
        coursePlanningService.planStudentsForCourses();
    }

    @Test
    @Transactional
    void update_student_name_requires_to_loadEntity() {
        var randomStudent = studentRepository.findAll().get(2);
        randomStudent.setName("Non-random-name");

        randomStudent = studentRepository.saveAndFlush(randomStudent);

        assertThat(randomStudent.getName()).isEqualTo("Non-random-name");
    }

    @Test
    @Transactional
    void use_modifying_query_to_update_name_clear_session_afterwards() {
        var id = studentRepository.findAll().get(3).getId();

        assertThat(studentRepository.updateStudentName(id, "Updated name")).isEqualTo(1);

        assertThat(studentRepository.getById(id).getName()).isEqualTo("Updated name");
    }

    @Test
    @Transactional
    void use_views_to_load_only_necessary_data() {
        var course = courseRepository.findAll().get(2);
        var student = studentRepository.findAll().get(2);

        //todo debug here
        System.out.println("Query names");
        var students = studentRepository.findAllStudentNamesAttendingCourse(course.getCourseName());
        System.out.println("Query named entity");
        var students2 = studentRepository.findByCourses_CourseName(course.getCourseName());
        System.out.println("Query view");
        var students3 = studentRepository.findStudentsByName(student.getName(), StudentView.class);
        System.out.println("Query view2");
        var students4 = studentRepository.findStudentsByName(student.getName(), StudentViewFull.class);

        assertThat(students.size()).isGreaterThan(0);
        var onlyNames = students2.stream().map(NamedEntity::getName).toList();
        assertThat(students).containsExactly(onlyNames.toArray(new String[0]));
        System.out.println(onlyNames);
        System.out.println(students3.get(0).formatToString());
    }

    @Test
    @Transactional
    void order_inserts() {
        for (int i = 0; i < 10; i++) {
            Course course = new Course();
            course.setCourseName("Test course " + i);
            var savedCourse = courseRepository.save(course);
            Student s1 = new Student();
            s1.setName("TestStudent " + i);
            s1.setCourses(Collections.singletonList(savedCourse));
            studentRepository.save(s1);
        }
        courseRepository.flush();
    }


    @Test
    @Transactional
    void use_query_with_paging() {
        var course = courseRepository.findAll().get(2);

        Pageable page = Pageable.ofSize(3).withPage(0);
        var students = studentRepository.findAllStudentNamesAttendingCourse(course.getCourseName(), page);

        assertThat(students.size()).isEqualTo(3);
    }

    @Test
    @Transactional
    void count_approx() {
        Pageable page = Pageable.ofSize(2);
        var course = studentRepository.findByName("Peter", page);
        if (course.size() == 0) {
            System.out.println("No students");
        } else if (course.size() == 1) {
            System.out.println("Unique student found");
        } else {
            System.out.println("Non-unique student found");
        }
    }

    @Test
    void fetch_and_pagination_problem() {
        studentRepository.findAllStudentsWithCuratorByName("Some name", Pageable.ofSize(10));
    }
    @Test
    @Transactional
    void long_running_transaction() throws InterruptedException {

        Long courseId = courseRepository.findAll().get(10).getId();

        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        new Thread(() -> {courseService.waitForCourseToFinish(courseId); }).start();
        Thread thread = new Thread(() -> {
            courseService.waitForCourseToFinish(courseId);
        });
        thread.start();
        thread.join();
    }

    @Test
    void l2_cache() throws InterruptedException {
        var cachedId = getCourseId();
        populateCache();
        var t = new Thread(() -> {
            transactionTemplate.setReadOnly(true);
            transactionTemplate.executeWithoutResult((status) -> {
                System.out.println("Loading from cache");
                var cachedInstance = lecturerRepository.getById(cachedId);
                System.out.println("Cached " + cachedInstance);
                cachedInstance.getCourses().forEach((course -> {
                    System.out.println("Cached course " + course);
                }));
            });
        });
        t.start();
        t.join();
    }

    @Test
    void query_cache() throws InterruptedException {
        var position = lecturerRepository.findAll().get(0).getPosition();
        var lecturers = lecturerRepository.getLecturersByPosition(position);
        System.out.println("Non cached count " + lecturers.size());

        System.out.println("Loading from query cache");
        var cachedLecturers = lecturerRepository.getLecturersByPosition(position);
        System.out.println("Cached count " + cachedLecturers.size());
        var otherLecturer = lecturerRepository.findAll().get(1);
        otherLecturer.setPosition("Another position");
        lecturerRepository.saveAndFlush(otherLecturer);

        System.out.println("Loading from query cache second time");
        var nonCachedResult = lecturerRepository.getLecturersByPosition(position);
        System.out.println("Cached count " + nonCachedResult.size());
    }






    @Test
    @Transactional
    void optimistic_locking() {
        var randomLecturer = lecturerRepository.findAll().get(0);
        randomLecturer.setName("I've changed my mind");
        lecturerRepository.saveAndFlush(randomLecturer);

        Lecturer recreated = new Lecturer();
        recreated.setId(randomLecturer.getId());
        recreated.setPosition(randomLecturer.getPosition());
        recreated.setName(randomLecturer.getName());
        recreated.setCourses(randomLecturer.getCourses());
        assertThatThrownBy(() -> lecturerRepository.saveAndFlush(recreated))
                .hasCauseInstanceOf(StaleObjectStateException.class);
    }

    @Test
    @Transactional
    void pessimistic_locking() throws InterruptedException {
        coursePlanningService.planStudentsForCourses();
        var lecturerName = lecturerRepository.findAll().get(0).getName();
        var lockedInstance = lecturerRepository.getByName(lecturerName);

        new Thread(() -> {
            transactionTemplate.executeWithoutResult(status -> {
                var loadedInstance = lecturerRepository.getByName(lecturerName);
                System.out.println("Loaded instance in thread " + loadedInstance);
            });
        }).start();
        Thread.sleep(3_000);
    }





    @Test
    @Transactional
    void use_criteria_api() {
        var courses = courseRepository.findCourseByName("Some name");

        assertThat(courses.size()).isEqualTo(0);
    }

    @Test
    @Transactional
    void many_to_many_graph() {
        var randomStudent = studentRepository.findAll().get(1);
        System.out.println(randomStudent);

        assertThat(randomStudent.getCourses()).isNotEmpty();
        randomStudent.getCourses().forEach(course -> {
            assertThat(course.getLecturer()).isNotNull();
        });
    }

    private void populateCache() {
        transactionTemplate.execute((status) -> courseRepository.findAll() );
    }

    private Long getCourseId() {
        return transactionTemplate.execute((status) -> {
            Lecturer lecturer = lecturerRepository.findAll().get(0);
            lecturer.getCourses().size();
            return lecturer.getId();
        });
    }


    TestRestTemplate restTemplate = new TestRestTemplate();
    class UserRequest {

        public UserRequest(String userName) {

        }
    }
    class UserInfo {
        public UserInfo(String userName, Object token) {

        }
    }
    interface UserRepository extends JpaRepository<UserInfo, Long> {

    }
    private UserRepository userRepository;

    @Transactional
    void performUserRegistration(String userName) {
        var token = restTemplate.postForEntity(
                "/create-token",
                new UserRequest(userName),
                UserResponse.class);
        userRepository.save(new UserInfo(userName, token.getBody()));
    }
}
class UserResponse {}
