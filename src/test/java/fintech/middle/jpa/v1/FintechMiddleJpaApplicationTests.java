package fintech.middle.jpa.v1;

import fintech.middle.jpa.dao.v1.StudentRepository;
import fintech.middle.jpa.model.v1.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FintechMiddleJpaApplicationTests {
	@Autowired
	StudentRepository studentRepository;

	@Test
	void create_then_load_new_student() {
		Student s = new Student();
		s.setName("Ivan");
		Student savedStudent = studentRepository.save(s);

		assertThat(savedStudent.getId()).isGreaterThan(0L);

		assertThat(studentRepository.getById(savedStudent.getId())).isNotNull();
		assertThat(studentRepository.findById(savedStudent.getId())).isPresent();

		studentRepository.save(new Student());
		assertThat(studentRepository.findAll().size()).isEqualTo(2);

		studentRepository.deleteById(savedStudent.getId());
		assertThat(studentRepository.findById(savedStudent.getId())).isNotPresent();
	}


	@Test
	void find_student_by_name() {
		Student s = new Student();
		s.setName("Ivan");
		Student savedStudent = studentRepository.save(s);

		Student foundStudent = studentRepository.findByName("Ivan");

		assertThat(foundStudent.getId()).isEqualTo(savedStudent.getId());
	}

	@Test
	void find_student_by_name_and_creation_date_sorted_by_name() {
		Student s = new Student();
		s.setName("Ivan");
		Student savedStudent = studentRepository.saveAndFlush(s);

		Student foundStudent = studentRepository.findByNameAndCreationDateBeforeOrderByName("Ivan", now());

		assertThat(foundStudent.getId()).isEqualTo(savedStudent.getId());
	}

}
