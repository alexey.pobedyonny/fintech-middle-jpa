package fintech.middle.jpa.v4;

import fintech.middle.jpa.dao.v4.StudentRepositoryV4;
import fintech.middle.jpa.model.v4.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.support.TransactionTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FintechMiddleJpaApplicationTests {
    @Autowired
    StudentRepositoryV4 studentRepository;
    @Autowired
    TransactionTemplate transactionTemplate;

    @Test
    void last_modified_changes() {
        Student auditedStudent = new Student();
        auditedStudent.setName("Name1");
        var savedStudent = studentRepository.saveAndFlush(auditedStudent);

        savedStudent.setName("Name2");
        var updatedStudent = studentRepository.saveAndFlush(savedStudent);

        assertThat(updatedStudent.getCreationDate()).isEqualTo(savedStudent.getCreationDate());
        assertThat(updatedStudent.getModifyDate()).isAfter(savedStudent.getModifyDate());
        assertThat(savedStudent.getCreatedBy()).isEqualTo("Current user name");
        assertThat(updatedStudent.getModifiedBy()).isEqualTo("Current user name");

    }

    @Test
    void create_history_record() {
        Student auditedStudent = new Student();
        auditedStudent.setName("Name1");
        var savedStudent = studentRepository.saveAndFlush(auditedStudent);

        savedStudent.setName("Name2");
        studentRepository.saveAndFlush(savedStudent);

        var revisions = studentRepository.findRevisions(auditedStudent.getId());
        revisions.forEach(System.out::println);
    }


}
